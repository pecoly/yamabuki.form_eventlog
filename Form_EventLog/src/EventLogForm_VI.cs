﻿using System;

using Yamabuki.Core;
using Yamabuki.Window.ChildForm;

namespace Yamabuki.Form.EventLog
{
    public interface EventLogForm_VI
        : ChildForm_VI
    {
        event Action<Boolean> Form_VisibleChanged;

        Action Form_Load { set; }
        
        Boolean FormVisible { get; }
        
        Boolean ErrorButtonChecked { get; set; }
        
        Boolean WarnButtonChecked { get; set; }

        Boolean InfoButtonChecked { get; set; }

        void InvokeForm(Action action);

        void AddEventLog(
            DateTime dateTime,
            String source,
            LogLevel logLevel,
            EventId eventId,
            String message);

        void ClearEventLog();
    }
}
