﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Xml.Linq;

using Yamabuki.Core;
using Yamabuki.EditorDev;
using Yamabuki.EditorDev.Manager;
using Yamabuki.Utility.Cast;
using Yamabuki.Utility.Log;
using Yamabuki.Utility.Xml;
using Yamabuki.Window.ChildForm;
using Yamabuki.Window.ParentForm;

namespace Yamabuki.Form.EventLog
{
    public class EventLogForm_P
        : ChildForm_P<EventLogForm_VI>
    {
        private const String DirectoryName = "Yamabuki.Form.EventLog";

        private const String ConfigFileName = "Config.xml";

        /// <summary>ロガー</summary>
        private static readonly Logger Logger = LogManager.GetLogger(
            MethodBase.GetCurrentMethod().DeclaringType);

        private Boolean isInitialized;

        private EventLogManager eventLogManager;

        private String configFilePath;

        private Boolean eventLogLevelError;

        private Boolean eventLogLevelWarn;

        private Boolean eventLogLevelInfo;

        public EventLogForm_P(ParentForm_PI parentForm_P)
            : base(parentForm_P)
        {
        }

        public override event Action<Boolean> Form_VisibleChanged
        {
            add { this.View.Form_VisibleChanged += value; }
            remove { this.View.Form_VisibleChanged -= value; }
        }

        public static ChildForm_PI Create(ParentForm_PI parentForm_PI, DevContext context)
        {
            var presenter = new EventLogForm_P(parentForm_PI);
            var view = new EventLogForm_V();
            presenter.eventLogManager = context.EventLogManager;
            presenter.View = view;
            return presenter;
        }

        public override void Load()
        {
            this.InitializeConfig();

            this.LoadConfig();
        }
        
        public override void Unload()
        {
            this.eventLogLevelError = this.View.ErrorButtonChecked;
            this.eventLogLevelWarn = this.View.WarnButtonChecked;
            this.eventLogLevelInfo = this.View.InfoButtonChecked;

            this.SaveConfig();
        }

        protected void AddEventLog(String source, LogLevel eventLevel, EventId eventId, String message)
        {
            var action = new Action(() => this.View.AddEventLog(
                DateTime.Now,
                source,
                eventLevel,
                eventId,
                message));

            this.View.InvokeForm(action);
        }

        protected void ClearEventLog()
        {
            this.View.ClearEventLog();
        }

        protected override void OnViewSet()
        {
            if (this.isInitialized)
            {
                Debug.Assert(false);
                return;
            }

            this.isInitialized = true;

            base.OnViewSet();

            this.View.Form_Load = this.Form_Load;
        }

        protected void Form_Load()
        {
            this.View.Initialize();

            this.eventLogManager.EventLogAdded += this.AddEventLog;
            this.eventLogManager.EventLogCleared += this.ClearEventLog;

            this.View.ErrorButtonChecked = this.eventLogLevelError;
            this.View.WarnButtonChecked = this.eventLogLevelWarn;
            this.View.InfoButtonChecked = this.eventLogLevelInfo;

            foreach (var eventLogInfo in this.eventLogManager.EventLogInfoList)
            {
                this.AddEventLog(
                    eventLogInfo.Source,
                    eventLogInfo.LogLevel,
                    eventLogInfo.EventId,
                    eventLogInfo.Message);
            }
        }

        protected virtual void InitializeConfig()
        {
            var configDirectoryPath = Path.Combine(this.PluginDirectoryPath, DirectoryName);
            this.configFilePath = Path.Combine(configDirectoryPath, ConfigFileName);
            if (!Directory.Exists(configDirectoryPath))
            {
                Directory.CreateDirectory(configDirectoryPath);
            }
        }

        protected virtual void LoadConfig()
        {
            this.eventLogLevelError = true;
            this.eventLogLevelWarn = true;
            this.eventLogLevelInfo = true;

            if (!File.Exists(this.configFilePath))
            {
                return;
            }

            var root = XElement.Load(this.configFilePath);

            this.eventLogLevelError = CastUtils.ToBoolean(
                XmlUtils.GetElementValue(root, Property.ErrorButtonChecked), x => false);
            this.eventLogLevelWarn = CastUtils.ToBoolean(
                XmlUtils.GetElementValue(root, Property.WarnButtonChecked), x => false);
            this.eventLogLevelInfo = CastUtils.ToBoolean(
                XmlUtils.GetElementValue(root, Property.InfoButtonChecked), x => false);
        }

        protected virtual void SaveConfig()
        {
            if (!String.IsNullOrEmpty(this.configFilePath))
            {
                var e = new XElement(
                    Property.Root,
                    new XElement(Property.ErrorButtonChecked, this.eventLogLevelError),
                    new XElement(Property.WarnButtonChecked, this.eventLogLevelWarn),
                    new XElement(Property.InfoButtonChecked, this.eventLogLevelInfo));
                XmlUtils.Save(this.configFilePath, e);
            }
        }

        private struct Property
        {
            public const String Root = "Root";
            public const String ErrorButtonChecked = "ErrorButtonChecked";
            public const String WarnButtonChecked = "WarnButtonChecked";
            public const String InfoButtonChecked = "InfoButtonChecked";
        }
    }
}
