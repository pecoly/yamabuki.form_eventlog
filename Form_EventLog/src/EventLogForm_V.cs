﻿using System;
using System.Data;
using System.Windows.Forms;

using WeifenLuo.WinFormsUI.Docking;

using Yamabuki.Core;
using Yamabuki.Window.ChildForm;

namespace Yamabuki.Form.EventLog
{
    public partial class EventLogForm_V
        : ChildForm_V, EventLogForm_VI
    {
        private EventHandler form_Load;

        private DataTable table = new DataTable();

        public EventLogForm_V()
        {
            this.InitializeComponent();
        }

        public override String PersistString
        {
            get { return "Yamabuki.Form.EventLog.EventLogForm"; }
        }

        public Action Form_Load
        {
            set
            {
                this.Load -= this.form_Load;
                this.form_Load = (sender, e) => value();
                this.Load += this.form_Load;
            }
        }

        public Boolean FormVisible
        {
            get { return this.Visible; }
        }

        public Boolean ErrorButtonChecked
        {
            get { return this.errorButton.Checked; }
            set { this.errorButton.Checked = value; }
        }

        public Boolean WarnButtonChecked
        {
            get { return this.warnButton.Checked; }
            set { this.warnButton.Checked = value; }
        }

        public Boolean InfoButtonChecked
        {
            get { return this.infoButton.Checked; }
            set { this.infoButton.Checked = value; }
        }

        public override DockState DefaultState
        {
            get { return DockState.DockBottom; }
        }

        public void InvokeForm(Action action)
        {
            this.Invoke(action);
        }

        public override void Initialize()
        {
            this.DoubleBuffered = true;

            this.errorButton.CheckedChanged += this.CheckedChanged;
            this.warnButton.CheckedChanged += this.CheckedChanged;
            this.infoButton.CheckedChanged += this.CheckedChanged;

            this.table.Columns.Add(Property.Level, typeof(String));
            this.table.Columns.Add(Property.DateTime, typeof(String));
            this.table.Columns.Add(Property.Source, typeof(String));
            this.table.Columns.Add(Property.Id, typeof(String));
            this.table.Columns.Add(Property.Detail, typeof(String));
        }

        public void AddEventLog(
            DateTime dateTime,
            String source,
            LogLevel eventLevel,
            EventId eventId,
            String message)
        {
            if (!this.IsTarget(eventLevel))
            {
                return;
            }

            var level = eventLevel.ToString();
            var subitems = new String[]
            {
                dateTime.ToString(),
                source,
                eventId.ToString(),
                message,
            };

            var item = new ListViewItem(level);
            item.SubItems.AddRange(subitems);
            this.eventLogListView.Items.Add(item);

            this.table.Rows.Add(new String[] { level, subitems[0], subitems[1], subitems[2], subitems[3] });
        }

        public void ClearEventLog()
        {
            this.eventLogListView.Items.Clear();
            this.table.Rows.Clear();
        }

        private Boolean IsTarget(LogLevel eventLevel)
        {
            return
                (eventLevel == LogLevel.Error && this.errorButton.Checked) ||
                (eventLevel == LogLevel.Warn && this.warnButton.Checked) ||
                (eventLevel == LogLevel.Info && this.infoButton.Checked);
        }

        private void CheckedChanged(Object sender, EventArgs e)
        {
            this.eventLogListView.Items.Clear();
            foreach (DataRow dr in this.table.Rows)
            {
                var level = dr[Property.Level] as String;
                var isTarget =
                    (level == LogLevel.Error.ToString() && this.errorButton.Checked) ||
                    (level == LogLevel.Warn.ToString() && this.warnButton.Checked) ||
                    (level == LogLevel.Info.ToString() && this.infoButton.Checked);

                if (isTarget)
                {
                    var subitems = new String[]
                    {
                        dr[Property.DateTime] as String,
                        dr[Property.Source] as String,
                        dr[Property.Id] as String,
                        dr[Property.Detail] as String,
                    };

                    var item = new ListViewItem(level);
                    item.SubItems.AddRange(subitems);
                    this.eventLogListView.Items.Add(item);
                }
            }
        }

        private struct Property
        {
            public const String Level = "LEVEL";
            public const String DateTime = "DATETIME";
            public const String Source = "SOURCE";
            public const String Id = "ID";
            public const String Detail = "DETAIL";
        }
    }
}
