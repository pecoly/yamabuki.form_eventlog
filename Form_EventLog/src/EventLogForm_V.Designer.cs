﻿namespace Yamabuki.Form.EventLog
{
    partial class EventLogForm_V
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if ( disposing && (components != null) )
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.eventLogListView = new Yamabuki.Window.ControlEx.DoubleBufferedListView();
            this.errorButton = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.warnButton = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.infoButton = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.SuspendLayout();
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "レベル";
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "日時";
            this.columnHeader1.Width = 150;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "ソース";
            this.columnHeader3.Width = 200;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "イベントID";
            this.columnHeader4.Width = 120;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "詳細";
            this.columnHeader5.Width = 450;
            // 
            // eventLogListView
            // 
            this.eventLogListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.eventLogListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader1,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.eventLogListView.Location = new System.Drawing.Point(14, 44);
            this.eventLogListView.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.eventLogListView.Name = "eventLogListView";
            this.eventLogListView.Size = new System.Drawing.Size(1042, 429);
            this.eventLogListView.TabIndex = 0;
            this.eventLogListView.UseCompatibleStateImageBehavior = false;
            this.eventLogListView.View = System.Windows.Forms.View.Details;
            // 
            // errorButton
            // 
            this.errorButton.Location = new System.Drawing.Point(14, 12);
            this.errorButton.Name = "errorButton";
            this.errorButton.Size = new System.Drawing.Size(90, 25);
            this.errorButton.TabIndex = 1;
            this.errorButton.Values.Text = "エラー";
            // 
            // warnButton
            // 
            this.warnButton.Location = new System.Drawing.Point(110, 12);
            this.warnButton.Name = "warnButton";
            this.warnButton.Size = new System.Drawing.Size(90, 25);
            this.warnButton.TabIndex = 2;
            this.warnButton.Values.Text = "警告";
            // 
            // infoButton
            // 
            this.infoButton.Location = new System.Drawing.Point(206, 12);
            this.infoButton.Name = "infoButton";
            this.infoButton.Size = new System.Drawing.Size(90, 25);
            this.infoButton.TabIndex = 3;
            this.infoButton.Values.Text = "情報";
            // 
            // EventLogForm_V
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1071, 493);
            this.Controls.Add(this.infoButton);
            this.Controls.Add(this.warnButton);
            this.Controls.Add(this.errorButton);
            this.Controls.Add(this.eventLogListView);
            this.DockAreas = ((WeifenLuo.WinFormsUI.Docking.DockAreas)(((((WeifenLuo.WinFormsUI.Docking.DockAreas.DockLeft | WeifenLuo.WinFormsUI.Docking.DockAreas.DockRight) 
            | WeifenLuo.WinFormsUI.Docking.DockAreas.DockTop) 
            | WeifenLuo.WinFormsUI.Docking.DockAreas.DockBottom) 
            | WeifenLuo.WinFormsUI.Docking.DockAreas.Document)));
            this.Font = new System.Drawing.Font("メイリオ", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "EventLogForm_V";
            this.ShowIcon = false;
            this.Text = "イベントログ";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private Window.ControlEx.DoubleBufferedListView eventLogListView;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton errorButton;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton warnButton;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton infoButton;


    }
}